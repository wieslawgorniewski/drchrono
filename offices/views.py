from django.http import HttpResponse
from drchrono.helpers import DrChronoView
import json


class Offices(DrChronoView):
    base_path = 'api/offices'

    def get(self, request):
        response = self.get_request(self.base_path)
        return HttpResponse(
            response.content,
            status=response.status_code,
            content_type='application/json'
        )
