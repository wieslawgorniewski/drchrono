from django.http import HttpResponse
from drchrono.helpers import DrChronoView
import json


class Appointments(DrChronoView):
    base_path = 'api/appointments'

    def get(self, request):
        response = self.get_request(self.base_path)
        return HttpResponse(
            response.content,
            response.code,
            content_type='application/json'
        )

    def post(self, request):
        response = self.post_request(path=self.base_path, data=json.loads(request.body))
        return HttpResponse(response.status_code)
