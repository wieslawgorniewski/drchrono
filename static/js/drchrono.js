var drChronoApp = angular.module('drChronoApp', ['ngResource', 'ngRoute', 'ui.bootstrap.datetimepicker']);

drChronoApp.config(function ($routeProvider) {
    $routeProvider.
        when('/step1/', { controller: 'PatientsCtrl', templateUrl: '/static/js/templates/patients.html' }).
        when('/step2/', { controller: 'DemographicsCtrl', templateUrl: '/static/js/templates/demographics.html' }).
        when('/step3/', { controller: 'AppointmentsCtrl', templateUrl: '/static/js/templates/appointments.html' }).
//        when('/settings/', { controller: 'MainSettingsCtrl', templateUrl: FavPagesAppTemplates.mainTemplatesFolder + 'ng_settings.html' }).
        otherwise({ redirectTo: '/step1/' });
});

drChronoApp.config(function($resourceProvider) {
    $resourceProvider.defaults.stripTrailingSlashes = false;
});

drChronoApp.factory('Api', ['$resource', function ($resource) {
    return {
         Patients: $resource('/patients/:id/', { id: '@id' }, { patch: { method: 'PATCH' }}),
         Appointments: $resource('/appointments/:id/', { id: '@id' }),
         Offices: $resource('/offices/:id/', { id: '@id' }, {}),
         Doctors: $resource('/doctors/:id/', { id: '@id' }, {}),
     };
}]);

drChronoApp.factory('Shared', function(){
  return {
    patient: JSON.parse(localStorage.getItem('patient')),
    offices: JSON.parse(localStorage.getItem('offices')),
    doctors: JSON.parse(localStorage.getItem('doctors'))
  };
});

drChronoApp.factory('SharedFunctions', function ($http, $routeParams, $location) {
    var factory = {};

    factory.logout = function () {
        localStorage.removeItem('patient');
        $location.path('/step1/');
    };

    factory.copyProperties = function (objectToFill, objectToCopy, properties) {
        for (var key in objectToCopy) {
            if (properties.indexOf(key) != -1 ) {
                objectToFill[key] = objectToCopy[key];
            }
        }
        return objectToFill;
    }

    return factory;
});

drChronoApp.run(['$routeParams', '$route', 'Api', function ($routeParams, $route, Api) {
    var getResources = function(resource_name) {
        Api[resource_name].get(
            function success(data) {
                if (data['results'] && data['results'].length > 0) {
                    localStorage.setItem(resource_name.toLowerCase(), JSON.stringify(data['results']));
                }
            },
            function err(data) {
                console.log(status);
        });
    };
    getResources('Offices');
    getResources('Doctors');
}]);

drChronoApp.controller('PatientsCtrl', function ($scope, $routeParams, $location, $route, Api, Shared) {
    $scope.shared = Shared;
    $scope.error = false;
    $scope.noResults = false;

    $scope.getPatient = function () {
        Api.Patients.get($scope.shared.patient,
        function success(data) {
            if (data['results'] && data['results'].length == 1) {
                $scope.error = false;
                $scope.noResults = false;
                $scope.shared.patient = data['results'][0];
                localStorage.setItem('patient', JSON.stringify($scope.shared.patient));
                $location.path('/step2/');
            } else {
                $scope.noResults = true;
            }
        },
        function err(data) {
            $scope.error = true;
        });
    };

    $scope.shared.patient = {};

    if (localStorage.getItem('patient')) {
        $location.path('/step2/');
    }
});

drChronoApp.controller('DemographicsCtrl', function ($scope, $routeParams, $location, $route, Api, Shared, SharedFunctions) {
    $scope.shared = Shared;
    $scope.sharedFunctions = SharedFunctions;
    $scope.demographics = {};
    $scope.errors = null;

    $scope.updateDemographics = function () {
        Api.Patients.patch({ id: $scope.shared.patient.id }, $scope.demographics,
        function success(data) {
            $scope.errors = null;
            $scope.shared.patient = data;
            localStorage.setItem('patient', JSON.stringify($scope.shared.patient));
            $location.path('/step3/');
        },
        function err(data) {
            $scope.errors = data.data;
        });
    };

    $scope.skipStep = function() {
        $location.path('/step3/');
    }

    if (!localStorage.getItem('patient')) {
        $location.path('/step1/');
    }

    $scope.demographics = $scope.sharedFunctions.copyProperties(
        {},
        $scope.shared.patient,
        ['zip_code', 'doctor', 'gender', 'city', 'emergency_contact_name', 'emergency_contact_phone']
    )
});

drChronoApp.controller('AppointmentsCtrl', function ($scope, $routeParams, $location, $route, Api, Shared, SharedFunctions) {
    $scope.shared = Shared;
    $scope.sharedFunctions = SharedFunctions;
    $scope.appointment = {};
    $scope.created = false;

    $scope.createAppointment = function () {
        Api.Appointments.save($scope.appointment,
        function success(data) {
            $scope.created = true;
        },
        function err(data) {

        });
    }

//    $scope.getAppointments = function () {
//        Api.Appointments.query(
//        function success(data) {
//            console.log(data);
//        },
//        function err(data) {
//
//        });
//    }

    $scope.onTimeSet = function (newDate, oldDate) {
        var localFormat = 'YYYY-MM-DD[T]HH:mm:ss';
        var ignoreTimezoneDate = moment(newDate).format(localFormat);
        $scope.appointment.scheduled_time = ignoreTimezoneDate;
    }

    if (!localStorage.getItem('patient')) {
        $location.path('/step1/');
    }
});













