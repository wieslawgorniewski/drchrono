from social.backends.oauth import BaseOAuth2
from settings import DR_CHRONO_BASE_URL
import os


class DrChronoOAuth2(BaseOAuth2):
    """DrChrono OAuth authentication backend"""
    name = 'drchrono'
    AUTHORIZATION_URL = '{base_url}/o/authorize'.format(base_url=DR_CHRONO_BASE_URL)
    ACCESS_TOKEN_URL = '{base_url}/o/token/'.format(base_url=DR_CHRONO_BASE_URL)
    SCOPE_SEPARATOR = ','
    ACCESS_TOKEN_METHOD = 'POST'
    EXTRA_DATA = [
        # ('id', 'id'),
        # ('expires', 'expires')
    ]

    def get_user_details(self, response):
        """Return user details from DrChrono account"""
        return {
            'username': response.get('login'),
            'email': response.get('email') or '',
            'first_name': response.get('name')
        }

    # def user_data(self, access_token, *args, **kwargs):
    #
    #     url = 'https://api.github.com/user?' + urlencode({
    #         'access_token': access_token
    #     })
    #     try:
    #         return json.load(self.urlopen(url))
    #     except ValueError:
    #         return None

    # from oauth.py
    def get_key_and_secret(self):
        return os.environ['DRCHRONO_KEY'], os.environ['DRCHRONO_SECRET']

    # from base.py
    def get_redirect_uri(self, state=None):
        return 'http://localhost:8000/complete/drchrono/'
