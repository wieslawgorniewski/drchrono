from drchrono.settings import DR_CHRONO_BASE_URL
import urllib
import requests
from social.apps.django_app.default.models import UserSocialAuth
from django.views.generic import View


class DrChronoView(View):
    def get_auth_token(self):
        user_social_auth = UserSocialAuth.objects.last()
        return user_social_auth.extra_data['access_token'] if user_social_auth else None

    # TODO: Change urlib2 to request to be consistent with second method from here
    def get_request(self, path, params={}):
        base_url = '{base}/{path}?'.format(base=DR_CHRONO_BASE_URL, path=path)
        for k, v in params.iteritems():
            if v and isinstance(v, list):
                params[k] = v[0]
        params['access_token'] = self.get_auth_token()
        final_url = base_url + urllib.urlencode(params)
        response = requests.get(final_url)
        return response

    def post_request(self, path, data={}):
        base_url = '{base}/{path}?'.format(base=DR_CHRONO_BASE_URL, path=path)
        final_url = base_url + urllib.urlencode({'access_token': self.get_auth_token()})
        response = requests.post(final_url, data=data, allow_redirects=True)
        return response

    def patch_request(self, path, data={}):
        base_url = '{base}/{path}?'.format(base=DR_CHRONO_BASE_URL, path=path)
        final_url = base_url + urllib.urlencode({'access_token': self.get_auth_token()})
        response = requests.patch(final_url, data=data, allow_redirects=True)
        return response
