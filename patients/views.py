from django.http import HttpResponse
import json
from drchrono.helpers import DrChronoView


class Patients(DrChronoView):
    base_path = 'api/patients'

    def patch(self, request, pk):
        response = self.patch_request(
            path='{}/{}'.format(self.base_path, pk),
            data=json.loads(request.body)
        )
        return HttpResponse(status=response.status_code, content=response.content)

    def get(self, request):
        params = {
            'first_name': request.GET.get('first_name', None),
            'last_name': request.GET.get('last_name', None)
        }
        response = self.get_request(self.base_path, params)
        return HttpResponse(
            response.content,
            status=response.status_code,
            content_type='application/json'
        )
